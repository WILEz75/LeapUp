﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallControl : MonoBehaviour {

    public Rigidbody rig;
    public float jumpForce=3;
    public bool aTouch;
    bool jumped;
    public bool oNground;
    int points;
    int oldBestPoint;
    public bool dead;
    public PhysicMaterial normalPhisMat;
    public PhysicMaterial onDeadPhisMat;
    static Transform lastPlatTouched;
    int left;
    AudioSource jumpAudio;
    public Vector3 jumpRotation = new Vector3(0.01f, 0.01f, 0.05f);
    public float lateralForce = 1;
    Animator ballAnima;
    public float onAirAnSpeed = 1;
    public float onGroundAnSpeed = 0.1f;

    void Awake () {
        points = 0;

        // GetComponent<SphereCollider>().material = normalPhisMat;
        jumpAudio = GetComponent<AudioSource>();
        ballAnima = GetComponent<Animator>();
    }


    private void Start()
    {
        GameManager.instance.pointText.text = "0";
        dead = false;
        GameManager.instance.deadPanel.SetActive(false);
        oldBestPoint = PlayerPrefs.GetInt(GameManager.AppName + "bestPoint");
        rig.angularVelocity = Vector3.zero;
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("platform"))
            oNground = false;
    }

    void ControlsUpdate()
    {
        aTouch = false;
        aTouch = Input.GetMouseButton(0);

    }

    public void Thouch() {

        if (Time.timeSinceLevelLoad < 1) return;
        if (Time.timeScale < 0.5f) return;
        if (oNground)
        {
            if (IstancingPlatforms.instance.isLeft) left = -1; else left = 1;
            transform.SetParent(null);
            rig.AddForce((Vector3.up * jumpForce * 10)+ (Vector3.left* left* lateralForce));
            rig.AddRelativeTorque(new Vector3(Random.Range(jumpRotation.x, -jumpRotation.x), Random.Range(jumpRotation.y, -jumpRotation.y), Random.Range(jumpRotation.z, -jumpRotation.z)));
            jumped = true;
            jumpAudio.Play();
            IstancingPlatforms.instance.NewPlatform();
            oNground = false;
            points++;
            GameManager.instance.pointText.text = points.ToString();
          if(ballAnima)  ballAnima.SetFloat("speed", onAirAnSpeed);

            if (points>5 && !GameManager.instance.alreadyShowed)
            if (points > oldBestPoint)
            {

                GameManager.instance.newRecordUI.SetActive(true);
                GameManager.instance.alreadyShowed = true;
            }
        }

        GameManager.instance.pointsMessages.transform.GetChild(0).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(1).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(2).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(3).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(4).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(5).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(6).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(7).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(8).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(9).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(10).gameObject.SetActive(false);
        GameManager.instance.pointsMessages.transform.GetChild(11).gameObject.SetActive(false);


        if (points == 10)
            GameManager.instance.pointsMessages.transform.GetChild(0).gameObject.SetActive(true);
        if (points == 20)
            GameManager.instance.pointsMessages.transform.GetChild(1).gameObject.SetActive(true);
        if (points == 30)
            GameManager.instance.pointsMessages.transform.GetChild(2).gameObject.SetActive(true);
        if (points == 40)
            GameManager.instance.pointsMessages.transform.GetChild(3).gameObject.SetActive(true);
        if (points == 50)
            GameManager.instance.pointsMessages.transform.GetChild(4).gameObject.SetActive(true);
        if (points == 60)
            GameManager.instance.pointsMessages.transform.GetChild(5).gameObject.SetActive(true);
        if (points == 100)
            GameManager.instance.pointsMessages.transform.GetChild(6).gameObject.SetActive(true);
        if (points == 150)
            GameManager.instance.pointsMessages.transform.GetChild(7).gameObject.SetActive(true);
        if (points == 200)
            GameManager.instance.pointsMessages.transform.GetChild(8).gameObject.SetActive(true);
        if (points == 250)
            GameManager.instance.pointsMessages.transform.GetChild(9).gameObject.SetActive(true);
        if (points == 300)
            GameManager.instance.pointsMessages.transform.GetChild(10).gameObject.SetActive(true);
        if (points == 500)
            GameManager.instance.pointsMessages.transform.GetChild(11).gameObject.SetActive(true);


    }


    private void OnTriggerEnter(Collider other)
    {
        

        if (other.tag.Equals("platform"))
        {
           
            if (other.transform.Equals(lastPlatTouched) && jumped) {
                print("DEAD for lastPlatTouched");
                if (Time.timeSinceLevelLoad > 2)
                Dead();
                return;
            }

            jumped = false;
            oNground = true;
            lastPlatTouched = other.transform;
            transform.SetParent(other.transform.parent.parent);
            return;
        }

        if (Time.timeSinceLevelLoad > 2) 
        if (other.tag.Equals("deadTrigger"))
        {
            print("DEAD" + (other.transform ));
            Dead();
        }


            
    }


    void Dead() {
        if (Time.timeSinceLevelLoad < 2) return;
        dead = true;


        GameManager.instance.deadPanel.SetActive(true);

        if (points> oldBestPoint)
        {
            PlayerPrefs.SetInt(GameManager.AppName + "bestPoint",points);

        }
        GameManager.instance.totalPoints = PlayerPrefs.GetInt(GameManager.AppName + "totalPoint")+ points;
        PlayerPrefs.SetInt(GameManager.AppName + "totalPoint", GameManager.instance.totalPoints);
        GameManager.instance.totalCoinsText.text = GameManager.instance.totalPoints.ToString();
        GameManager.instance.totalCoinsText2.text = GameManager.instance.totalPoints.ToString();
        GameManager.instance.totalCoinsText3.text = GameManager.instance.totalPoints.ToString();


        GameManager.instance.EndGame(points);
        GameManager.instance.startGame = false;


        //foreach (GameObject plats in IstancingPlatforms.instance.platforms)
        //{
        //  Destroy(plats);
        //}
    }

    void Update()
    {


        if (!GameManager.instance.startGame) return;
        if (oNground && !dead)
        {
            rig.angularVelocity = Vector3.zero;
           if(ballAnima) ballAnima.SetFloat("speed", onGroundAnSpeed);
        }


    }


}
