﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class GameManager : MonoBehaviour
{


    public static string AppName = "LeapUp";
    public bool UseEnglish;
    public bool removePostEffects;
    public Text pointText;
    public Text sharePointText;
    public static BallControl ballControl;
    public GameObject deadPanel;
    public GameObject newBallsPanel;
    public GameObject creditsPanel;
    public GameObject fadeOutPanel;
    public GameObject fadeInPanel;
    public static GameManager instance;
    public GameObject[] ballPrefabs;
    public string selectedBallString;
    public GameObject selectedBall;
    public int bestPoint;
    public bool startGame;
    public GameObject mainMenu;
    public GameObject Level;
    public Text endGamePointsText2;
    public Text endGameBestPointsText;
    public Text endGameBestPointsText2;
    public int totalPoints;
    public Text totalCoinsText;
    public Text totalCoinsText2;
    public Text totalCoinsText3;
    public Text buildVersionText;
    public Image colorImage;
    public Renderer BkRend;
    public Light dirLight;
    public Color[] bkColors;
    Color col;
    Color targetCol;
    public int colorNumber;
    public Behaviour HQEffect;
    public GameObject selectionImage;
    public Button[] ballsButtons;
    public GameObject noCoinsMessage;
    float myTimer;
    public static AudioSource audioMusic;
    public AudioClip[]musics;
    public static int restartInt; //Quante volte è stato restartato
    static bool isRestart;
    public GameObject AdButton;
    public GameObject []OnlyOnline;
    public IstancingPlatforms istancingPlatforms;
    public GameObject newRecordUI;
    public bool alreadyShowed;
    public GameObject pausePanel;
    public Text pauseTimeUI;
    int pause = 3;
    float pauseTimer;
    bool startPauseTimer;
    public GameObject[] decalsPrefabs;
    public Image musicIcon;
    public GameObject guideMain;
    public int totalePalle;
    public int palleInPossesso;
    public Text palleInPossessoText;
    public GameObject pointsMessages;

    void Awake()
    {
        instance = this;
        //PlayerPrefs.DeleteAll();
        Level.SetActive(false);
        startGame = false;
        audioMusic = GetComponent<AudioSource>();
        mainMenu.SetActive(true);
        deadPanel.SetActive(false);
        newBallsPanel.SetActive(false);
        creditsPanel.SetActive(false);
        noCoinsMessage.SetActive(false);
        fadeOutPanel.SetActive(false);
        fadeInPanel.SetActive(true);
        pausePanel.SetActive(false);
        newRecordUI.SetActive(false);
        guideMain.SetActive(false);
        audioMusic.clip = musics[Random.Range(0, musics.Length)];
        Screen.sleepTimeout = (int)0f;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;


    }

    void Start()
    {
        System.GC.Collect();

        Application.targetFrameRate = 60;

        if (FindObjectOfType<BallControl>())
            Destroy(FindObjectOfType<BallControl>().gameObject);

        col = bkColors[0];
        UpdatePoints();
        StartCoroutine(CheckConnection());


        //carica palla scelta
        if (PlayerPrefs.HasKey(AppName + "selectedBallString"))
        {
            selectedBallString = PlayerPrefs.GetString(AppName + "selectedBallString");
            foreach (Button bb in ballsButtons)
            {

                totalePalle++;
                if (PlayerPrefs.HasKey(AppName + bb.name))
                { //Palla in possesso
                    bb.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                    palleInPossesso++;
                }
                else
                {
                    bb.GetComponent<Image>().color = new Color(0.35f, 0.3f, 0.2f, 0.35f);
                }

                if (bb.name.Equals(selectedBallString))
                    selectionImage.transform.position = bb.transform.position;

            }

        }
        else
        {
            selectedBallString = "Ball01";
            PlayerPrefs.SetString(AppName + "selectedBallString", selectedBallString);
            PlayerPrefs.SetInt(AppName + "Ball01", 1);

            foreach (Button bb in ballsButtons)
            {

                totalePalle++;
                if (PlayerPrefs.HasKey(AppName + bb.name))
                { //Palla in possesso
                    bb.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                    palleInPossesso++;
                }
                else
                {
                    bb.GetComponent<Image>().color = new Color(0.35f, 0.3f, 0.2f, 0.35f);
                }


                if (bb.name.Equals(selectedBallString))
                    selectionImage.transform.position = bb.transform.position;

            }
        }


        palleInPossessoText.text = palleInPossesso.ToString();


        if ((Random.Range(0, 3) == 0)|| (Application.internetReachability.Equals(NetworkReachability.NotReachable)))
        AdButton.SetActive(false);

        foreach (GameObject balls in ballPrefabs)
        {
            if (balls.name.Equals(selectedBallString))
            {
                selectedBall = Instantiate(balls);
            }
        }


        selectedBall.gameObject.SetActive(false);


        ballControl = selectedBall.GetComponent<BallControl>();
        FindObjectOfType<SmootFollow>().target = selectedBall.transform;

        buildVersionText.text = "v." + (Application.version);

        ballControl.dead = false;



        if (PlayerPrefs.HasKey(AppName + "HI"))
            {
            if (PlayerPrefs.GetInt(AppName + "HI").Equals(1))
            {
                if(!removePostEffects)
                HQEffect.enabled = true;
                dirLight.shadows = LightShadows.Hard;
            }
            else
            {
                HQEffect.enabled = false;
                dirLight.shadows = LightShadows.None;

            }
            }
            else
            {
            if (!isRestart)
                StartCoroutine(RescaleQuality());
            }



        if (isRestart) //Se è un restart esegui il gioco all'avvio
        {
            restartInt++;

            if (Application.internetReachability != NetworkReachability.NotReachable)
             if (restartInt == 3 || restartInt == 5 || restartInt > 6)
            {
                Advertisement.Show();
            }

           StartCoroutine(AutoStartGame());

            print(restartInt);
        }


        audioMusic.Play();

    }


    IEnumerator CheckConnection() {
        yield return new WaitForSeconds(1);
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            foreach (GameObject go in OnlyOnline)
            {
                go.SetActive(false);
            }
        }
    }


   public void ShowVideoForReward() { }

    IEnumerator RescaleQuality()
    {
        yield return new WaitForSeconds(3);

        if (ExplosionsFPS.fps > 30)
        {
            if (!removePostEffects)
            HQEffect.enabled = true;
            dirLight.shadows = LightShadows.Hard;
            PlayerPrefs.SetInt(AppName + "HI", 1);
        }
        else
        {
            HQEffect.enabled = false;
            dirLight.shadows = LightShadows.None;
            PlayerPrefs.SetInt(AppName + "HI", 0);
        }

        print("rescaleQuality - fps:"+ ExplosionsFPS.fps);
    }


    public void NoAudio() {

        if (audioMusic.volume >= 0.5)
        {
            audioMusic.volume = 0;
            musicIcon.color = new Color(0.9f, 0.9f, 0.2f, 0.2f);
        }
        else
        {
            audioMusic.volume = 0.5f;
            musicIcon.color = new Color(0.9f, 0.9f, 0.2f, 0.8f);
        }

    }

    public void Restart()
    {
        fadeOutPanel.SetActive(true);
        isRestart = true;
        StartCoroutine(Reload());

    }

    IEnumerator Reload() {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void EndGame(int points)
    {
        startGame = false;
        endGamePointsText2.text = points.ToString();
        sharePointText.text = points.ToString();
        endGameBestPointsText.text= PlayerPrefs.GetInt(AppName + "bestPoint").ToString();       
        DisableLevel();
    }

    void DisableLevel()
    {
   
       Level.SetActive(false);
       Destroy(selectedBall);
    }

    void StartGame()
    {
        mainMenu.SetActive(false);
        ballControl.dead = false;
        startGame = true;
        Level.SetActive(true);
        selectedBall.gameObject.SetActive(true);
        isRestart = false;
        targetCol = bkColors[colorNumber];
        Instantiate(decalsPrefabs[Random.Range(0, decalsPrefabs.Length)]);

        StartCoroutine(InstanceFirst());
    }


    //Attesa per il tutorial
    IEnumerator InstanceFirst()
    {
        ShowGuide();
           yield return new WaitForSeconds(4);
        istancingPlatforms.NewPlatform();
    }


    void ShowGuide() {
        guideMain.SetActive(true);
    }

        IEnumerator AutoStartGame()
    {
        ballControl.dead = false;
        mainMenu.SetActive(false);
        isRestart = false;
        Level.SetActive(true);
        selectedBall.gameObject.SetActive(true);
        Instantiate(decalsPrefabs[Random.Range(0, decalsPrefabs.Length)]);  
        startGame = true;             
        targetCol = bkColors[colorNumber];
        yield return new WaitForSeconds(1);
        istancingPlatforms.NewPlatform();

    }


    public void QuitGame() {
        Application.Quit();
    }

    public void Thouch()
    {
        if (Time.timeScale > 0)
            ballControl.Thouch();
    }

    public void ReturnToMenu()
    {
        startGame = false;
        mainMenu.SetActive(true);
        Level.SetActive(false);
       // selectedBall.gameObject.SetActive(false);
        mainMenu.SetActive(true);
        deadPanel.SetActive(false);
        isRestart = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void PauseGame()
    {
        if (Time.timeScale > 0)
        {
            Time.timeScale = 0;
            pausePanel.SetActive(true);
            pauseTimeUI.gameObject.SetActive(false);
        }
        else {
            pauseTimer = 4;
            pauseTimeUI.text = "3";
            startPauseTimer = true;
            pauseTimeUI.gameObject.SetActive(true);

        }
    }



    public void OpenCredits()
    {
       creditsPanel.SetActive(true);
    }

    void UpdatePoints() {


        if (PlayerPrefs.HasKey(AppName + "totalPoint"))
            totalPoints = PlayerPrefs.GetInt(AppName + "totalPoint");

        if (PlayerPrefs.HasKey(AppName + "bestPoint"))
            bestPoint = PlayerPrefs.GetInt(AppName + "bestPoint");

        
        endGameBestPointsText2.text = bestPoint.ToString();


        totalCoinsText.text = totalPoints.ToString();
        totalCoinsText2.text = totalPoints.ToString();
        totalCoinsText3.text = totalPoints.ToString();

    }

    private void OnApplicationQuit()
    {
        isRestart = false;
    }


    public void OpenNewBallsPanel()
    {
        newBallsPanel.SetActive(true);
    }

    void ChangeTargetColor()
    {
        myTimer += Time.deltaTime;

        col = Color.Lerp(col, targetCol, Time.deltaTime * 0.4f);
        BkRend.material.color=  dirLight.color = col;
        colorImage.color = col * 1.4f;

        if (myTimer >= 10)
        {
            if (colorNumber == bkColors.Length - 1)
                colorNumber = 0;
            else
                colorNumber++;

            targetCol = bkColors[colorNumber];

            myTimer = 0;

        }

    }




    public void BallSelection(GameObject ball)
    {
        if (PlayerPrefs.HasKey(AppName + ball.name)) //Se acquistata
        {

            selectionImage.transform.position = ball.transform.position;
            selectedBallString = ball.name;
            PlayerPrefs.SetString(AppName + "selectedBallString", selectedBallString);


        }
        else
        {

            TryToUlockBall(ball);

        }


    }

    void TryToUlockBall(GameObject ball)
    {
        if (totalPoints >= 500)
        {
            PlayerPrefs.SetInt(AppName + ball.name, 1);
            ball.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            totalPoints -= 500;
            PlayerPrefs.SetInt(AppName + "totalPoint", totalPoints);
            totalCoinsText.text = totalPoints.ToString();
            totalCoinsText2.text = totalPoints.ToString();
            totalCoinsText3.text = totalPoints.ToString();
            BallSelection(ball);
        }
        else {
            noCoinsMessage.SetActive(true);
        }
    }


    public void AddCoinsFromStore(int coins) {
        totalPoints += coins;
        PlayerPrefs.SetInt(AppName + "totalPoint", totalPoints);
        totalCoinsText.text = totalPoints.ToString();
        totalCoinsText2.text = totalPoints.ToString();
        totalCoinsText3.text = totalPoints.ToString();
    }






    void Update() {
        if (ballControl.dead)
            GameManager.audioMusic.volume -= 0.15f * Time.deltaTime;
        else
        ChangeTargetColor();

        if (startPauseTimer)
        {

            pauseTimer -= Time.unscaledDeltaTime;
            if (pauseTimer < 3.2f)
                pauseTimeUI.text = Mathf.RoundToInt(pauseTimer).ToString();


            if (pauseTimer<=0) //endPause
            {
                Time.timeScale = 1;
                pausePanel.SetActive(false);
                startPauseTimer = false;
                pauseTimer = 4;
            }
        }


        //Back Buttoon on mobile device
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (creditsPanel.activeInHierarchy|| newBallsPanel.activeInHierarchy)
            ReturnToMenu();

        }


    }









}
