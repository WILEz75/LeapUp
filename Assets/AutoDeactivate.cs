﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDeactivate : MonoBehaviour {

    public float time;

    private void Start()
    {

        if (time>0)
        Invoke("Deactivate", time);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }


}
